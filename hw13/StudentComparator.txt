package com.dad;

import java.util.Comparator;

public class StudentComparator implements Comparator {

    private int compareStudent(Student a, Student b) {
        if (a != null && b == null) {
            return 1;
        }
        if (a == null && b != null) {
            return -1;
        }
        if (a == null && b == null) {
            return 0;
        }
        return a.getLastName().compareTo(b.getLastName());
    }
}
