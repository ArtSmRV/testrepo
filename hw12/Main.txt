package com.gmail.sar;

public class Main {
    public static void main(String[] args) {
        Student st1 = new Student("Sergii", "Bardilev", 27, true, 5445, "1a");
        Student st2 = new Student("Bogdan", "Sokrut", 24, true, 1788, "1a");
        Student st3 = new Student("Sofia", "Lashchva", 25, false, 4587, "1a");
        Student st4 = new Student("Dmitro", "Shulegko", 23, true, 6543, "1a");
        Student st5 = new Student("Maria", "Kumanovska", 23, false, 7543, "1a");
        Student st6 = new Student("Anastasia", "Ilyushenko", 26, false, 3452, "1a");
        Student st7 = new Student("Vladislav", "Glushchenko", 24, true, 9645, "1a");
        Student st8 = new Student("Julia", "Marchenko", 25, false, 2345, "1a");
        Student st9 = new Student("Artur", "Smilianets", 24, true, 1236, "1a");
        Student st10 = new Student("Oleksandr", "Rosrtskii", 26, true, 5326, "1a");
        Student st11 = new Student("Tetyana", "Beseda", 22, false, 7845, "1a");

        Group gr = new Group("10a");
        try {
            gr.addStudent(st1);
            gr.addStudent(st2);
            gr.addStudent(st3);
            gr.addStudent(st4);
            gr.addStudent(st5);
            gr.addStudent(st6);
            gr.addStudent(st7);
            gr.addStudent(st8);
            gr.addStudent(st9);
            gr.addStudent(st10);
            gr.addStudent(st11);
        } catch (GroupOverflowException e) {
            System.out.println(e.getMessage());
        }
        System.out.println(gr);

        gr.removeStudent(3549);
        System.out.println(gr);

        System.out.println(gr.findStudentByLastName("Smilianets"));
        System.out.println(gr.findStudentByLastName("Kumanovskyy"));
    }

}
