package com.gmail.sar;

public class Student extends Human {
    private long studentId;
    private String groupName;

    public Student(String name, String lastName, int age, boolean sex, long studentId, String groupName) {
        super(name, lastName, age, sex);
        this.studentId = studentId;
        this.groupName = groupName;
    }

    public Student(String name, String lastName, int age, boolean sex) {
        super(name, lastName, age, sex);
    }

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return "Student [studentId=" + studentId + ", groupName=" + groupName + "]" + super.toString();
    }

}
