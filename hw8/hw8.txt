package com.gmail.sar;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class taskOne {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("enter your text: ");
        String text = sc.nextLine();

        File f1 = new File("text.txt");
        try(PrintWriter t1 = new PrintWriter(f1)){
            t1.println(text);
        }catch(IOException e) {
            e.printStackTrace();
        }
    }
}

package com.gmail.sar;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class taskTwo {
    public static void main(String[] args) {

        int[][] arr1 = {{33, 54, 76}, {12, 23, 23}};
        File text = new File("Arr1.txt");
        try {
            text.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        saveArr(text, arr1);
    }

    public static void saveArr(File file, int[][] array) {
        try (PrintWriter pw = new PrintWriter(file)) {
            for (int i = 0; i < array.length; i++) {
                for (int j = 0; j < array[i].length; j++) {
                    pw.print(array[i][j] + " ");
                }
                pw.println();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


package com.gmail.sar;

import java.io.File;

public class taskThree {
    public static void main(String[] args) {

        File file = new File(".");
        showDir(file);
    }

    public static void showDir(File a) {

        File[] dirrectory = a.listFiles();

        for (int i = 0; i < dirrectory.length; i++) {
            if(dirrectory[i].isDirectory())
                System.out.println(dirrectory[i].getName());
        }
    }
}
