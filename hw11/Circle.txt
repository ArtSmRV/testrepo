package com.gmail.www;

public class Circle extends Shape {
    private Point d;
    private Point e;

    public Circle(Point d, Point e) {
        this.d = d;
        this.e = e;
    }

    public Point getD() {
        return d;
    }

    public void setD(Point d) {
        this.d = d;
    }

    public Point getE() {
        return e;
    }

    public void setE(Point e) {
        this.e = e;
    }

    @Override
    double getPerimetrs() {
        double r = Math.sqrt(Math.pow((e.getX() - d.getX()), 2) + Math.pow((e.getY() - d.getY()), 2));
        double l = Math.PI * r;
        return l;
    }

    @Override
    double getArea() {
        double r = Math.sqrt(Math.pow((e.getX() - d.getX()), 2) + Math.pow((e.getY() - d.getY()), 2));
        double s = Math.PI * Math.pow(r, 2);
        return s;
    }


}
